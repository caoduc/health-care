import 'dart:async';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/views/doctor/history_categories_page.dart';
import 'package:flutter_app/views/general/login_with_google_page.dart';
import 'package:flutter_app/services/ggMap.dart';
import 'package:flutter_app/views/user/object_analyzer_page.dart';
import 'package:flutter_app/views/user/booking_history.dart';
import 'package:flutter_app/views/user/healing_page.dart';
import 'package:flutter_app/views/user/tricks_tips_page.dart';
import 'package:flutter_app/views/user/take_care_page.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserHomePage extends StatefulWidget {
  @override
  _UserHomePageState createState() => _UserHomePageState();
}

class _UserHomePageState extends State<UserHomePage>{

  String? _name, _photoUrl, _email;

  @override
  void initState() {
    getUserInfo();
    super.initState();
  }

  getUserInfo() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      _name = preferences.getString("user_name");
      _photoUrl = preferences.getString("user_photo");
      _email = preferences.getString("user_email");
    });
  }

  List<String> service = [
    "Chăm sóc điều dưỡng",
    "Thủ thuật điều dưỡng",
    "Phục hồi chức năng",
    "Lấy mẫu xét nghiệm tại nhà",
    "Đặt lịch tư vấn",
    "Lịch sử đặt lịch"
  ];

  List<String> image_user = [
    "assets/user/doctor.png",
    "assets/user/thuthuat.png",
    "assets/user/phuchoichucnang.png",
    "assets/user/xetnghiem.png",
    "assets/user/booking.png",
    "assets/user/calendar.png"
  ];

  List<String> slide = [
    "assets/images/hostpital.jpg",
    "assets/images/hostpital2.jpg",
    "assets/images/hostpital3.jpg",
  ];


  List<Widget>body = [
    TakeCarePage(),
    TricksTipsPage(),
    HealingPage(),
    ObjectAnalyzerPage(),
    HistoryCategoriesPage(),
    UserBookingHistory()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Color.fromRGBO(46, 204, 113, 1),
              ),
              child: Container(
                child: Column(
                  children: [
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: NetworkImage(_photoUrl!),
                          fit: BoxFit.contain
                        )
                      ),
                    ),
                    Text(
                      "" + _name!,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.white
                      ),
                    ),
                    Text(
                      "" + _email!,
                      style: TextStyle(
                          fontWeight: FontWeight.bold,color: Colors.white
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ListTile(
              title: Row(
                children: [
                  Icon(EvaIcons.homeOutline,color: Colors.green, size: 35,),
                  SizedBox(width: 10,),
                  Text("Trang chủ", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  Spacer(),
                  Icon(Icons.arrow_forward_ios)
                ],
              ),
              onTap: () {
                // Update the state of the app.
                // ...

              },
            ),
            // ListTile(
            //   title: Row(
            //     children: [
            //       Icon(EvaIcons.person, color: Colors.amber, size: 35,),
            //       SizedBox(width: 10,),
            //       Text("Cá nhân", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
            //       Spacer(),
            //       Icon(Icons.arrow_forward_ios)
            //     ],
            //   ),
            //   onTap: () {
            //     // Update the state of the app.
            //     // ...
            //
            //   },
            // ),

            ListTile(
              title: Row(
                children: [
                  Icon(EvaIcons.questionMarkCircle, color: Colors.lightBlue, size: 35,),
                  SizedBox(width: 10,),
                  Text("Phản hồi", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  Spacer(),
                  Icon(Icons.arrow_forward_ios)
                ],
              ),
              onTap: () {
                // Update the state of the app.
                // ...

              },
            ),
            Container(
              height: 300,
            ),
            ListTile(
              title: Row(
                children: [
                  Icon(EvaIcons.logOutOutline,color: Colors.redAccent, size: 35,),
                  SizedBox(width: 10,),
                  Text("Đăng xuất", style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
                  Spacer(),
                  Icon(Icons.arrow_forward_ios)
                ],
              ),
              onTap: (){
                gooleSignout(context);
              },
            ),
          ],
        ),
      ),
      appBar: AppBar(
        title: Text("BlueCare",style: TextStyle(color: (Colors.white),fontWeight: FontWeight.bold),),
        centerTitle: true,
        //backgroundColor: Colors.white,
      ),
      backgroundColor: Color.fromRGBO(234, 237, 237, 1),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
//Trình chiều slide
            CarouselSlider.builder(
                options: CarouselOptions(
                  height: 200,
                  viewportFraction: 1,
                  initialPage: 0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 3),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enlargeCenterPage: true,
                  scrollDirection: Axis.horizontal,
                ),
                itemCount: slide.length,
                itemBuilder: (BuildContext context, int itemIndex, int){
                return Container(
                  height: 200,
                  width: MediaQuery.of(context).size.width,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage(slide[itemIndex]),
                        fit: BoxFit.cover
                      )
                  )
                );
              }
            ),

            Expanded(

              child: ListView.builder(

                itemCount: service.length,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(
                          MaterialPageRoute(builder:(route)=>body[index]
                          )
                      );
                      print('Tapped ${index}');
                    },

                      child: listItem(image_user[index], service[index], context)
                  );

                },
              ),
            )
          ],
        )
      )
    );
  }
}
//Thẻ
Widget listItem(String image, String title, BuildContext context) {
  return Container(

    width: double.infinity,
    margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white
    ),
    child: Row(
      children: [
        Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),

        SizedBox(width: 10,),

        Container(
          width: MediaQuery.of(context).size.width - 120,
          child: Text(
            title,
            //Chỉnh style service
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20,
              color: Colors.green,
              fontFamily: 'Raleway'
            ),
            textAlign: TextAlign.justify,
          ),
        )
      ],
    ),
  );
}


Future<void> gooleSignout(BuildContext context) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.clear();
  Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (route) => LoginWithGooglePage()));
}