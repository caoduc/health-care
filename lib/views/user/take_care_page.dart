//import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/views/user/home_booking_page.dart';
import 'package:flutter_app/views/user/hospital_booking_page.dart';
import 'package:flutter_app/views/user/oldster_booking_page.dart';
//import 'package:carousel_slider/carousel_slider.dart';
//import 'package:flutter_app/Forms/FormChamSocTaiNha.dart';

// ignore: must_be_immutable
class TakeCarePage extends StatelessWidget {

  List<String> service = [
    "Chăm sóc bệnh nhân tại nhà",
    "Chăm sóc bệnh nhân tại bệnh viện",
    "Chăm sóc người già tại nhà"
    // "Lấy mẫu xét nghiệm tại nhà",
    // "Đặt lịch tư vấn"
  ];

  List<String> image_care = [
    "assets/user/doctor.png",
    "assets/user/thuthuat.png",
    "assets/user/phuchoichucnang.png",
    "assets/user/xetnghiem.png",
    "assets/user/booking.png",
    "assets/user/calendar.png"
  ];


  List<Widget>body = [
    HomeBookingPage(),
    HospitalBookingPage(),
    OldsterBookingPage(),
  ];
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Chăm sóc điều dưỡng", style: TextStyle(
              color: (Colors.white), fontWeight: FontWeight.bold),),
          centerTitle: true,
          //backgroundColor: Colors.white,
        ),
        backgroundColor: Color.fromRGBO(234, 237, 237, 0.8),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: service.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: (){
                        Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder:(route)=>body[index]
                            )
                        );
                        print('Tapped ${index}');
                      },
                        child: listItem(image_care[index], service[index], context)
                    );
                  },
                ),
              )
            ],
          ),
        )
    );
  }
}

Widget listItem(String image, String title, BuildContext context) {
  return Container(
    width: double.infinity,
    margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white
    ),
    child: Row(
      children: [
        Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),

        SizedBox(width: 10,),

        Container(
          width: MediaQuery.of(context).size.width - 120,
          child: Text(
            title,
            //Chỉnh style service
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.green,
                fontFamily: 'Raleway'
            ),
            textAlign: TextAlign.justify,
          ),
        ),
      ],
    ),
  );
}