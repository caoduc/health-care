import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/views/doctor/history_categories_page.dart';
import 'package:flutter_app/views/general/login_with_google_page.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'booking_list_page.dart';

// ignore: must_be_immutable
class DoctorHomePage extends StatelessWidget {
  var service = ["Danh sách đơn đặt lịch", "Lịch sử nhận đơn đặt lịch"];

  var image_doctor = ["assets/doctor/list.png", "assets/doctor/calendar.png"];
  var slide = ["assets/images/hostpital.jpg"];

  List<Widget> body = [BookingListPage(), HistoryCategoriesPage()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Drawer(
          child: ListView(
            children: [
              DrawerHeader(
                child: Text(""),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(46, 204, 113, 1),
                ),
              ),
              ListTile(
                title: Row(
                  children: [
                    Icon(
                      EvaIcons.homeOutline,
                      color: Colors.green,
                      size: 35,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Trang chủ",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios)
                  ],
                ),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              ListTile(
                title: Row(
                  children: [
                    Icon(
                      EvaIcons.person,
                      color: Colors.amber,
                      size: 35,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Cá nhân",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios)
                  ],
                ),
                onTap: () {
                  // Update the state of the app.
                  // ...
                },
              ),
              Container(
                height: 300,
              ),
              ListTile(
                title: Row(
                  children: [
                    Icon(
                      EvaIcons.logOutOutline,
                      color: Colors.redAccent,
                      size: 35,
                    ),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      "Đăng xuất",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                    ),
                    Spacer(),
                    Icon(Icons.arrow_forward_ios)
                  ],
                ),
                onTap: () async {
                  gooleSignout(context);
                },
              ),
            ],
          ),
        ),
        appBar: AppBar(
          title: Text(
            "BlueCare",
            style:
                TextStyle(color: (Colors.white), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          //backgroundColor: Colors.white,
        ),
        backgroundColor: Color.fromRGBO(234, 237, 237, 0.8),
        body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: Column(
              children: [
//Trình chiều slide
                CarouselSlider.builder(
                    options: CarouselOptions(
                      height: 200,
                      viewportFraction: 1,
                      initialPage: 0,
                      enableInfiniteScroll: true,
                      reverse: false,
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 3),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      enlargeCenterPage: true,
                      scrollDirection: Axis.horizontal,
                    ),
                    itemCount: slide.length,
                    itemBuilder: (BuildContext context, int itemIndex, int){
                      return Container(
                          height: 200,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(slide[itemIndex]),
                                  fit: BoxFit.cover
                              )
                          )
                      );
                    }
                ),

                Expanded(
                  child: ListView.builder(
                    itemCount: service.length,
                    itemBuilder: (context, index) {
                      return GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (route) => body[index]));
                            //print('Tapped ${index}');
                          },
                          child: listItem(
                              image_doctor[index], service[index], context));
                    },
                  ),
                )
              ],
            )));
  }
}

//Thẻ
Widget listItem(String image, String title, BuildContext context) {
  return Container(
    width: double.infinity,
    margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0), color: Colors.white),
    child: Row(
      children: [
        Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
          width: MediaQuery.of(context).size.width - 120,
          child: Text(
            title,
            //Chỉnh style service
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.green,
                fontFamily: 'Raleway'),
            textAlign: TextAlign.justify,
          ),
        )
      ],
    ),
  );
}

Future<void> gooleSignout(BuildContext context) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.clear();
  Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (route) => LoginWithGooglePage()));
}
