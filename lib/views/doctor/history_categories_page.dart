import 'package:flutter/material.dart';
import 'package:flutter_app/views/doctor/history_booking_confirmed_page.dart';



// ignore: must_be_immutable
class HistoryCategoriesPage extends StatelessWidget {

  static const service = [
    "Chăm sóc bệnh nhân tại nhà",
    "Chăm sóc bệnh nhân tại bệnh viện",
    "Chăm sóc người già tại nhà"
  ];

  static const image_care = [
    "assets/doctor/list.png",
    "assets/doctor/calendar.png",
    "assets/doctor/calendar.png"
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Đơn đặt lịch", style: TextStyle(
              color: (Colors.white), fontWeight: FontWeight.bold),),
          centerTitle: true,
          //backgroundColor: Colors.white,
        ),
        backgroundColor: Color.fromRGBO(234, 237, 237, 0.8),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Expanded(
                child: ListView.builder(
                  itemCount: service.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                        onTap: (){
                          Navigator.of(context).push(
                              MaterialPageRoute(builder:(route)=> BookConfirmHistoryPage(categoryID: index)
                              )
                          );
                          // if(index == 0) {
                          //   Navigator.of(context).push(MaterialPageRoute(builder: (route)=> confirmTainhaPage()));
                          // }
                        },
                        child: listItem(image_care[index], service[index], context));
                  },
                ),
              )
            ],
          ),
        )
    );
  }
}

Widget listItem(String image, String title, BuildContext context) {
  return Container(
    width: double.infinity,
    margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0),
        color: Colors.white
    ),
    child: Row(
      children: [
        Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),

        SizedBox(width: 10,),

        Container(
          width: MediaQuery.of(context).size.width - 120,
          child: Text(
            title,
            //Chỉnh style service
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.green,
                fontFamily: 'Raleway'
            ),
            textAlign: TextAlign.justify,
          ),
        ),
      ],
    ),
  );
}