//import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/models/book_details_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
//import 'package:carousel_slider/carousel_slider.dart';

class HomeBookingListPage extends StatefulWidget {
  @override
  _HomeBookingListPageState createState() => _HomeBookingListPageState();
}

class _HomeBookingListPageState extends State<HomeBookingListPage> {
  bool _isLoading = true;
  CollectionReference users =
      FirebaseFirestore.instance.collection('ChamSocTaiNha');

  @override
  void initState() {
    super.initState();
  }

  deleteData(String id) async {
    await FirebaseFirestore.instance
        .collection('ChamSocTaiNha')
        .doc(id)
        .delete()
        .onError((error, stackTrace) {
      print('error');
    });
  }

  _confirmDataFromFirebase(BookDetailsModel datLich) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String? userId = preferences.getString("user_id_doctor");

    print(datLich.position);

    Map<String, dynamic> _dataConfirm = {
      "name": datLich.name,
      "email": datLich.email,
      "phone": datLich.phone,
      "start_time": datLich.startTime,
      "end_time": datLich.endTime,
      "address": datLich.address,
      "note": datLich.note,
      "position": datLich.position
    };

    await FirebaseFirestore.instance
        .collection('ConfirmTainha')
        .doc(userId)
        .collection("DanhSachXacNhan")
        .doc(datLich.bookID)
        .set(_dataConfirm)
        .onError((error, stackTrace) {
      print('error');
    });

    await FirebaseFirestore.instance
        .collection('LichSuDat')
        .doc(datLich.customer)
        .collection("DanhSachChamSocTaiNha")
        .doc(datLich.bookID)
        .set(_dataConfirm)
        .onError((error, stackTrace) {
      print('error');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            "Đơn chăm sóc tại nhà",
            style:
                TextStyle(color: (Colors.white), fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          //backgroundColor: Colors.white,
        ),
        backgroundColor: Color.fromRGBO(234, 237, 237, 0.8),
        body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            children: [
              Expanded(
                  child: StreamBuilder(
                      stream: FirebaseFirestore.instance
                          .collection("ChamSocTaiNha")
                          .snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot>? snapshot) {
                        return snapshot!.connectionState !=
                                ConnectionState.waiting
                            ? snapshot.hasData == false
                                ? Center(
                                    child: Text("No data found"),
                                  )
                                : ListView.builder(
                                    itemCount: snapshot.data!.docs.length,
                                    itemBuilder: (context, index) =>
                                        GestureDetector(
                                          onTap: () {},
                                          child: Card(
                                            child: Column(
                                              children: <Widget>[
                                                Text(
                                                  "Tên khách hàng: ${snapshot.data!.docs[index].get('name')}",
                                                  style: TextStyle(
                                                      color: Colors.green,
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold),
                                                ),
                                                Text(
                                                    "Email: ${snapshot.data!.docs[index].get('email')}",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    "Số điện thoại: ${snapshot.data!.docs[index].get('phone')}",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    "Ngày giờ bắt đầu: ${snapshot.data!.docs[index].get('start_time')}",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    "Ngày giờ kết thúc: ${snapshot.data!.docs[index].get('end_time')}",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    "Địa chỉ: ${snapshot.data!.docs[index].get('address')}",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    "Ghi chú: ${snapshot.data!.docs[index].get('note')}",
                                                    style: TextStyle(
                                                        color: Colors.green,
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Container(
                                                  height: 50,
                                                ),
                                                GestureDetector(
                                                    onTap: () {
                                                      _confirmDataFromFirebase(new BookDetailsModel(
                                                          bookID: snapshot.data!
                                                              .docs[index].id,
                                                          name: snapshot
                                                              .data!.docs[index]
                                                              .get('name'),
                                                          email: snapshot
                                                              .data!.docs[index]
                                                              .get('email'),
                                                          phone: snapshot
                                                              .data!.docs[index]
                                                              .get('phone'),
                                                          startTime: snapshot
                                                              .data!.docs[index]
                                                              .get(
                                                                  'start_time'),
                                                          endTime: snapshot
                                                              .data!.docs[index]
                                                              .get('end_time'),
                                                          address: snapshot.data!.docs[index].get('address'),
                                                          note: snapshot.data!.docs[index].get('note'),
                                                          position: snapshot.data!.docs[index].get('position'),
                                                          customer: snapshot.data!.docs[index].get('customer')),
                                                        );
                                                      deleteData(snapshot.data!
                                                          .docs[index].id);
                                                    },
                                                    child: Container(
                                                        width: 120,
                                                        height: 40,
                                                        alignment:
                                                            Alignment.center,
                                                        decoration: BoxDecoration(
                                                            borderRadius:
                                                                BorderRadius
                                                                    .circular(
                                                                        8.0),
                                                            color:
                                                                Colors.green),
                                                        child: const Text(
                                                          "Xác nhận đơn",
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white,
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold),
                                                        ))),
                                                Container(
                                                  height: 25,
                                                ),
                                              ],
                                            ),
                                          ),
                                        ))
                            : Center(child: CircularProgressIndicator());
                      }))
            ],
          ),
        ));
  }
}

Widget listItem(String image, String title, BuildContext context) {
  return Container(
    width: double.infinity,
    margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
    padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8.0), color: Colors.white),
    child: Row(
      children: [
        Container(
          height: 60,
          width: 60,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8.0),
            image: DecorationImage(
              image: AssetImage(image),
              fit: BoxFit.cover,
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Container(
          width: MediaQuery.of(context).size.width - 120,
          child: Text(
            title,
            //Chỉnh style service
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
                color: Colors.green,
                fontFamily: 'Raleway'),
            textAlign: TextAlign.justify,
          ),
        ),
      ],
    ),
  );
}
