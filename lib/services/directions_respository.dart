import 'dart:convert';

import 'package:flutter_app/models/direction_model.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import '.env.dart';

class DirectionsRespository {
  String _baseURL = "https://maps.googleapis.com/maps/api/directions/json?";

  Future<Directions>? getDirections(
      {required LatLng origin, required LatLng destination}) async {
    final String originText = 'origin=${origin.latitude},${origin.longitude}&';
    final String destinationText =
        'destination=${destination.latitude},${destination.longitude}&';
    final String key = 'key=AIzaSyCaE42rX5alqkKmlTquag8kuNhWz1qi2fs';

    _baseURL = _baseURL + originText + destinationText + key;
    final response = await http.get(Uri.parse(_baseURL));

    print(response.statusCode);
    if (response.statusCode == 200) {
      final responseJson = json.decode(response.body);
      return Directions.fromMap(responseJson);
    }

    return Directions();
  }
}
