import 'package:cloud_firestore/cloud_firestore.dart';

class BookDetailsModel {
  BookDetailsModel(
      {this.bookID,
      this.name,
      this.phone,
      this.email,
      this.address,
      this.note,
      this.startTime,
      this.endTime,
      this.confirmed,
      this.position,
      this.customer});

  BookDetailsModel.fromJSON(Map<String, dynamic> json, String id) {
    this.bookID = id;
    this.name = json['name'];
    this.phone = json['phone'];
    this.email = json['email'];
    this.address = json['address'];
    this.note = json['note'];
    this.startTime = json['start_time'];
    this.endTime = json['end_time'];
    this.confirmed = json['confirmed'];
    this.position = json['position'];
    this.customer = json['customer'];
  }

  String? bookID;
  String? name;
  String? phone;
  String? email;
  String? address;
  String? note;
  String? startTime;
  String? endTime;
  String? customer;
  bool? confirmed;
  GeoPoint? position;
}
