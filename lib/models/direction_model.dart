import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Directions {
  LatLngBounds? bounds;
  List<PointLatLng>? polylinePoints;
  String? totalDistance;
  String? totalDuration;

  Directions(
      {this.bounds,
      this.polylinePoints,
      this.totalDistance,
      this.totalDuration});

  factory Directions.fromMap(Map<String, dynamic> map) {
    print(map);
    if ((map['routes'] as List).isNotEmpty) {
      final data = Map<String, dynamic>.from(map['routes'][0]);

      print("Northeast: ${data['bounds']['northEast']}");

      final northEast = data['bounds']['northeast'];
      final southWest = data['bounds']['southwest'];

      final bounds = LatLngBounds(
          northeast: LatLng(northEast['lat'], northEast['lng']),
          southwest: LatLng(southWest['lat'], southWest['lng']));

      String distance = "";
      String duration = "";
      if ((data['legs'] as List).isNotEmpty) {
        final leg = data['legs'][0];
        distance = leg['distance']['text'];
        duration = leg['duration']['text'];
      }

      return Directions(
          bounds: bounds,
          polylinePoints: PolylinePoints()
              .decodePolyline(data['overview_polyline']['points']),
          totalDistance: distance,
          totalDuration: duration);
    }

    return Directions();
  }
}
