class User {

  User({this.name, this.email, this.photoUrl});

  String? name;
  String? email;
  String? photoUrl;

}